# Changelog

### [1.3.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/compare/release/1.3.0...release/1.3.1) (2024-10-08)


### Bug Fixes

* deploy appVersion to 1.0.6 ([60eaba0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/60eaba0a4db1c0b3c6f3a20ad7e5b7b4649f0e41))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/compare/release/1.2.0...release/1.3.0) (2024-06-12)


### Features

* update helm chart appVersion to 1.0.5 ([638a3a5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/638a3a59fabea5bcd11dee6e902f046a50409359))


### Bug Fixes

* restore appversion to dev ([53e284a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/53e284a63403b9f40ed33d8bf486972517587360))
* restore testing tag ([dea3480](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/dea3480d70113522e855a7847a6a1636656c92c6))
* wait 5 minutes before killing pod ([5f6f942](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/5f6f942cebba02ce32903efe81083bf6cd5828b2))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-01-16)


### Features

* update app version to 1.0.4 ([ecc8bb7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/ecc8bb783708f7193c07619805e0de0667454d9c))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/compare/release/1.0.1...release/1.1.0) (2023-08-25)


### Features

* new helm for appversion  1.0.3 ([7bcefc2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/7bcefc2fdba6740805529d53d4b34da5dc44ca40))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/compare/release/1.0.0...release/1.0.1) (2023-05-09)


### Bug Fixes

* Add more description in readme ([18cf118](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/18cf1186f9709bcd8c588f552003a1d87cfcfaff))
* dev chart version deploy dev app version ([3954344](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/39543444a315ffdd0fe63d018f828f6867d8b1c9))
* publish new helm stable version ([b6b5402](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/b6b540286fd945f800e10b048c22c95f6ae4bc85))
* testing helm deploy testing app version ([be86585](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/frontnxt-helm-chart/commit/be8658511d16d08a8064837c16b902c2b95d7bf2))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([620e997](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/620e9978da5525b147618b6f7e1d34783fcca40c))
* **ci:** push helm package to chart repository ([f53e7d3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/f53e7d3cb0f2dab9fb82f9f785051751a8fe3f2a))
* **ci:** update helm chart version with `semantic-release` ([a519656](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/a519656af61363a1aeedfe7b2693713c5f701f76))
* **ci:** validate helm chart at `lint` stage ([662ff4f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/662ff4f10090da38533737f01831c055869ace1d))
* use chart.yaml as default version ([040beef](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/040beefd0b1602f7c677482682f5bd7975d49ab7))


### Bug Fixes

* update hpa api version removed on k8s 1.26 ([1431556](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/1431556e453e9aa0381f7f2c9b95e45666b7bae1))
* **values:** use image 'frontal-nextcloud:dev' ([90487c8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/90487c8d00c8711b46c9f966384059f1fc4791f4))


### Continuous Integration

* **commitlint:** enforce commit message format ([ee92285](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/ee922857b29b2ffc73c2a7b15cf22fd3846ae764))
* **release:** create release automatically with `semantic-release` ([6f2e645](https://gitlab.mim-libre.fr/EOLE/eole-3/services/frontnxt/commit/6f2e6456843b52faab94a48e5bf2c802815e5720))
